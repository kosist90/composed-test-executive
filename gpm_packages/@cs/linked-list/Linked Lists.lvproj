﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="GPM Packages" Type="Folder">
			<Property Name="GPM" Type="Bool">true</Property>
			<Item Name="@cs" Type="Folder">
				<Item Name="lookup-table" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="LookupTable" Type="Folder">
							<Item Name="LookupTable.lvlib" Type="Library" URL="../gpm_packages/@cs/lookup-table/Source/LookupTable/LookupTable.lvlib"/>
						</Item>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/lookup-table/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/lookup-table/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/lookup-table/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/lookup-table/README.md"/>
				</Item>
				<Item Name="variant" Type="Folder">
					<Item Name="Source" Type="Folder">
						<Item Name="VariantExtensions.lvlib" Type="Library" URL="../../variant/Source/VariantExtensions.lvlib"/>
					</Item>
					<Item Name="CHANGELOG.md" Type="Document" URL="../gpm_packages/@cs/variant/CHANGELOG.md"/>
					<Item Name="gpackage.json" Type="Document" URL="../gpm_packages/@cs/variant/gpackage.json"/>
					<Item Name="LICENSE" Type="Document" URL="../gpm_packages/@cs/variant/LICENSE"/>
					<Item Name="README.md" Type="Document" URL="../gpm_packages/@cs/variant/README.md"/>
				</Item>
			</Item>
		</Item>
		<Item Name="Sandbox" Type="Folder">
			<Item Name="Basic Linked List Test.vi" Type="VI" URL="../Source/Sandbox/Basic Linked List Test.vi"/>
			<Item Name="Pointer Test.vi" Type="VI" URL="../Source/Sandbox/Pointer Test.vi"/>
			<Item Name="Serialization Test.vi" Type="VI" URL="../Source/Sandbox/Serialization Test.vi"/>
		</Item>
		<Item Name="Linked Lists.lvlib" Type="Library" URL="../Source/Linked List/Linked Lists.lvlib"/>
		<Item Name="String Manipulation.lvlib" Type="Library" URL="../Source/String Manipulation/String Manipulation.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="LookupTable.lvlib" Type="Library" URL="../../lookup-table/Source/LookupTable/LookupTable.lvlib"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
